package com.samia.experiment.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.samia.experiment.R;
import com.samia.experiment.adapter.AdvancedRecyclerviewNotificationAdapter;
import com.samia.experiment.adapter.AdvertisingAdapter;
import com.samia.experiment.adapter.WalletDetailsAdapter;


import com.samia.experiment.model.AdvertisingItem;
import com.samia.experiment.model.item_wallet_details;

import java.util.ArrayList;

public class WalletDetailsActivity extends AppCompatActivity  {
    private com.samia.experiment.databinding.ActivityWalletDetailsBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_details);



        binding = com.samia.experiment.databinding.ActivityWalletDetailsBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        ArrayList<item_wallet_details> list = new ArrayList<>();
        list.add(new item_wallet_details("Nov","الخميس , نوفمبر 27","#89087678","Progcoin","03:31PM","-$ 60.00"));
        list.add(new item_wallet_details("Nov","الخميس , نوفمبر 27","#89087678","Progcoin","03:31PM","-$ 60.00"));

        WalletDetailsAdapter adapter = new WalletDetailsAdapter(list,WalletDetailsActivity.this);
        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,
                false);
        binding.walletDetailsActivityRv.setLayoutManager(manager);
        binding.walletDetailsActivityRv.setHasFixedSize(true);
        binding.walletDetailsActivityRv.setAdapter(adapter);


        binding.walletDetailsActivityBackImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        binding.ImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WalletDetailsActivity.this,EditprofileActivity.class);
                startActivity(intent);

            }
        });
    }


}
