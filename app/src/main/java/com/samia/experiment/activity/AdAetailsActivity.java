package com.samia.experiment.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.samia.experiment.R;
import com.samia.experiment.databinding.ActivityAdAetailsBinding;
import com.samia.experiment.model.AdvertisingItem;

public class AdAetailsActivity extends AppCompatActivity {


    private ActivityAdAetailsBinding binding;
    ImageView description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ad_aetails);
        binding = ActivityAdAetailsBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


    }
}