package com.samia.experiment.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.samia.experiment.R;
import com.samia.experiment.adapter.AdvertisingAdapter;

import com.samia.experiment.databinding.ActivityMainBinding;
import com.samia.experiment.fragment.BottomSheetFragment;
import com.samia.experiment.model.AdvertisingItem;
import com.samia.experiment.model.MySharedPreferences;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdvertisingAdapter.Intr {
    private ActivityMainBinding binding;
    ArrayList<AdvertisingItem> list;
    public Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        dialog = new Dialog(this);
        MySharedPreferences mySharedPreferences = new MySharedPreferences(this);
        setContentView(view);


        list = new ArrayList<>();
        list.add(new AdvertisingItem(R.drawable.crates_mounted, "Name Add", "Watch the ad", "1010", "01:02", 0));
        list.add(new AdvertisingItem(R.drawable.crates_mounted, "Name Add", "Watch the ad", "1010", "01:02", 1));
        list.add(new AdvertisingItem(R.drawable.crates_mounted, "Name Add", "Watch the ad", "1010", "01:02", 2));
        list.add(new AdvertisingItem(R.drawable.crates_mounted, "Name Add", "Watch the ad", "1010", "01:02", 3));
        list.add(new AdvertisingItem(R.drawable.crates_mounted, "Name Add", "Watch the ad", "1010", "01:02", 4));
        list.add(new AdvertisingItem(R.drawable.crates_mounted, "Name Add", "Watch the ad", "1010", "01:02", 5));


        AdvertisingAdapter adapter = new AdvertisingAdapter(list, this);
        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,
                false);
        binding.mainActivityRv.setLayoutManager(manager);
        binding.mainActivityRv.setHasFixedSize(true);
        binding.mainActivityRv.setAdapter(adapter);


        binding.mainActivityMenuImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MySharedPreferences.getvalue() == 2) {
                    openDialog();
                } else {
                    BottomSheetFragment bottomSheetFragment = new BottomSheetFragment();
                    bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
                }
            }
        });


        binding.mainActivityNotificationImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (MySharedPreferences.getvalue() == 2) {
                    openDialog();
                } else {

                    Intent intent = new Intent(MainActivity.this, NotificationsActivity.class);
                    startActivity(intent);

                }

            }
        });

        binding.mainActivityWalletImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this, WalletDetailsActivity.class);
                    startActivity(intent);



            }
        });


    }

    @Override
    public void vi(int position) {
        if (MySharedPreferences.getvalue() == 2) {
            openDialog();
        } else {
            Intent intent = new Intent(this,AdAetailsActivity.class);
            startActivity(intent);
        }
    }

    private void openDialog() {
        dialog.setContentView(R.layout.custom_dialog3);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button dialogLoginBtn = dialog.findViewById(R.id.dialogLoginBtn);
        dialog.show();
        dialogLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}