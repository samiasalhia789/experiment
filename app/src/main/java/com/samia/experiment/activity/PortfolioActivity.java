package com.samia.experiment.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.samia.experiment.R;
import com.samia.experiment.databinding.ActivityEditprofileBinding;
import com.samia.experiment.databinding.ActivityPortfolioBinding;
import com.samia.experiment.model.MySharedPreferences;

public class PortfolioActivity extends AppCompatActivity {
    private ActivityPortfolioBinding binding;
    Dialog dialog ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_portfolio);
        dialog  = new Dialog(this);

        binding = ActivityPortfolioBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);



        binding.portfolioActivityWithdrawalBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validationData()) {
                    openDialog();
                }

            }
        });


        binding.portfolioActivityBackImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void openDialog() {
        dialog.setContentView(R.layout.custom_dialog2);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button Ok = dialog.findViewById(R.id.okBtn);
        dialog.show();
    }

    private static boolean isValidEmail(CharSequence tSequence) {
        return (!TextUtils.isEmpty(tSequence) && Patterns.EMAIL_ADDRESS.matcher(tSequence).matches());
    }

    private boolean validationData() {
        if (binding.portfolioActivityFullNameBtn.getText().toString().isEmpty()) {
            Toast.makeText(PortfolioActivity.this, "يجب ادخال الإسم", Toast.LENGTH_SHORT).show();
            return false;
        }  else if (binding.portfolioActivityGenderEt.getText().toString().isEmpty()) {
            Toast.makeText(PortfolioActivity.this, "يجب ادخال الجنس", Toast.LENGTH_SHORT).show();

            return false;
        }
        else if (binding.portfolioActivityAddressEt.getText().toString().isEmpty()) {
            Toast.makeText(PortfolioActivity.this, "يجب ادخال العنوان", Toast.LENGTH_SHORT).show();

            return false;
        }
        else if (binding.portfolioActivityAPhoneEt.getText().toString().isEmpty()) {
            Toast.makeText(PortfolioActivity.this, "يجب ادخال الهاتف", Toast.LENGTH_SHORT).show();

            return false;
        }
        return true;
    }

}