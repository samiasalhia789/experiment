package com.samia.experiment.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.samia.experiment.R;
import com.samia.experiment.databinding.ActivityEditprofileBinding;
import com.samia.experiment.model.MySharedPreferences;


public class EditprofileActivity extends AppCompatActivity {
    private ActivityEditprofileBinding binding;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editprofile);
        dialog = new Dialog(this);

        binding = ActivityEditprofileBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.editprofileActivityPasswordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });


        binding.editProfileActivitySaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validationData()) {
                }

            }
        });

        binding.editPofileActivityBackImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    private void openDialog() {
        dialog.setContentView(R.layout.custom_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        EditText OldPasswordET = dialog.findViewById(R.id.OldPasswordET);
        EditText NewPasswordET = dialog.findViewById(R.id.NewPasswordET);
        EditText ConfirmPasswordET = dialog.findViewById(R.id.ConfirmPasswordET);
        Button SaveBtn = dialog.findViewById(R.id.SaveBtn);

        dialog.show();
    }


    private static boolean isValidEmail(CharSequence tSequence) {
        return (!TextUtils.isEmpty(tSequence) && Patterns.EMAIL_ADDRESS.matcher(tSequence).matches());
    }

    private boolean validationData() {
        if (binding.editPofileActivityGenderEt.getText().toString().isEmpty()) {
            Toast.makeText(EditprofileActivity.this, "يجب ادخال الإسم", Toast.LENGTH_SHORT).show();
            return false;
        } else if (binding.editPofileActivityEmailEt.getText().toString().isEmpty()) {
            Toast.makeText(EditprofileActivity.this, "يجب ادخال الايميل", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!isValidEmail(binding.editPofileActivityEmailEt.getText().toString())) {
            Toast.makeText(EditprofileActivity.this, "الايميل غير صالح", Toast.LENGTH_SHORT).show();

            return false;
        } else if (binding.editPofileActivityAgeEt.getText().toString().isEmpty()) {
            Toast.makeText(EditprofileActivity.this, "يجب ادخال كلمة المرور", Toast.LENGTH_SHORT).show();

            return false;
        } else if (binding.editPofileActivityGenderEt.getText().toString().isEmpty()) {
            Toast.makeText(EditprofileActivity.this, "يجب ادخال الجنس", Toast.LENGTH_SHORT).show();

            return false;
        }

        return true;
    }


}