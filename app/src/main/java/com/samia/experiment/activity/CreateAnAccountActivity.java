package com.samia.experiment.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Toast;

import com.samia.experiment.R;
import com.samia.experiment.databinding.ActivityCreateAnAccountBinding;

public class CreateAnAccountActivity extends AppCompatActivity {
    private ActivityCreateAnAccountBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_an_account);
        binding = ActivityCreateAnAccountBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.createAnAccountActivityLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validationData()) {
                    Intent intent = new Intent(CreateAnAccountActivity.this,LoginActivity.class);
                    startActivity(intent);
                }

            }
        });

        binding.createAnAccountTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent intent = new Intent(CreateAnAccountActivity.this,LoginActivity.class);
                    startActivity(intent);
            }
        });

    }

    private static boolean isValidEmail(CharSequence tSequence) {
        return (!TextUtils.isEmpty(tSequence) && Patterns.EMAIL_ADDRESS.matcher(tSequence).matches());
    }

    private boolean validationData() {
        if (binding.createAnAccountActivityNameET.getText().toString().isEmpty()) {
            Toast.makeText(CreateAnAccountActivity.this, "يجب ادخال الاسم", Toast.LENGTH_SHORT).show();
            return false;
        } else if (binding.createAnAccountActivityEmailET.getText().toString().isEmpty()) {
            Toast.makeText(CreateAnAccountActivity.this, "يجب ادخال الايميل", Toast.LENGTH_SHORT).show();

            return false;
        } else if (!isValidEmail(binding.createAnAccountActivityEmailET.getText().toString())) {
            Toast.makeText(CreateAnAccountActivity.this, "الايميل غير صالح", Toast.LENGTH_SHORT).show();

            return false;
        } else if (binding.createAnAccountActivityAgeET.getText().toString().isEmpty()) {
            Toast.makeText(CreateAnAccountActivity.this, "يجب ادخال العمر", Toast.LENGTH_SHORT).show();

            return false;
        } else if (binding.createAnAccountActivityGenderET.getText().toString().isEmpty()) {
            Toast.makeText(CreateAnAccountActivity.this, "يجب ادخال الجنس", Toast.LENGTH_SHORT).show();

            return false;
        } else if (binding.createAnAccountActivityPasswordET.getText().toString().isEmpty()) {
            Toast.makeText(CreateAnAccountActivity.this, "يجب ادخال كلمة المرور", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}