package com.samia.experiment.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.samia.experiment.R;
import com.samia.experiment.databinding.ActivityCreateAnAccountBinding;
import com.samia.experiment.databinding.ActivityLoginBinding;
import com.samia.experiment.model.MySharedPreferences;


public class LoginActivity extends AppCompatActivity {
    private ActivityLoginBinding binding;
    int LoginUsers = 1;
    int VisitorUsers = 2;

    public static final String loginUsers = "User";
    public static final String visitorUsers = "visitor";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        MySharedPreferences mySharedPreferences=new MySharedPreferences(this);
        setContentView(view);


        binding.loginActivityLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validationData()) {
                    MySharedPreferences.setvalue(1);
                    Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                    startActivity(intent);

                }


            }
        });

        binding.loginActivityVisitorBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              MySharedPreferences.setvalue(2);
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });


        binding.loginActivityCreateAnAccountBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, CreateAnAccountActivity.class);
                startActivity(intent);

            }
        });

    }


    private static boolean isValidEmail(CharSequence tSequence) {
        return (!TextUtils.isEmpty(tSequence) && Patterns.EMAIL_ADDRESS.matcher(tSequence).matches());
    }

    private boolean validationData() {
        if (binding.loginActivityEmailAddressET.getText().toString().isEmpty()) {
            Toast.makeText(LoginActivity.this, "يجب ادخال الايميل", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!isValidEmail(binding.loginActivityEmailAddressET.getText().toString())) {
            Toast.makeText(LoginActivity.this, "الايميل غير صالح", Toast.LENGTH_SHORT).show();

            return false;
        } else if (binding.loginActivityPasswordET.getText().toString().isEmpty()) {
            Toast.makeText(LoginActivity.this, "يجب ادخال كلمة المرور", Toast.LENGTH_SHORT).show();

            return false;
        }
        return true;
    }


}