package com.samia.experiment.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.samia.experiment.R;
import com.samia.experiment.adapter.AdvancedRecyclerviewNotificationAdapter;
import com.samia.experiment.adapter.AdvertisingAdapter;
import com.samia.experiment.databinding.ActivityAdAetailsBinding;
import com.samia.experiment.databinding.ActivityNotificationsBinding;
import com.samia.experiment.model.item_notification;

import java.util.ArrayList;

public class NotificationsActivity extends AppCompatActivity  {
    RecyclerView Recycler ;
    AdvancedRecyclerviewNotificationAdapter adapter;
    private ActivityNotificationsBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        binding = ActivityNotificationsBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        final ArrayList<item_notification> list = new ArrayList<>();

        list.add(new item_notification("Nov","الخميس , نوفمبر 27","#89087678","Progcoin","03:31PM","-$ 60.00","","",""));
        list.add(new item_notification("","","","","","","الخميس , نوفمبر 27","#89087678","Progcoin"));




        Log.d("TAG",  list.size()+"");

        AdvancedRecyclerviewNotificationAdapter adapter = new AdvancedRecyclerviewNotificationAdapter(list);

        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,
                false);
        binding.Recycler.setLayoutManager(manager);
        binding.Recycler.setHasFixedSize(true);
        binding.Recycler.setAdapter(adapter);



    }



}