package com.samia.experiment.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.samia.experiment.R;
import com.samia.experiment.activity.AdAetailsActivity;
import com.samia.experiment.model.AdvertisingItem;
import com.samia.experiment.model.item_wallet_details;

import java.util.ArrayList;

public class WalletDetailsAdapter extends RecyclerView.Adapter<WalletDetailsAdapter.MyHolder> {

    ArrayList<item_wallet_details> itemWalletDetails;
    Context context;
    private Activity activity;

    public WalletDetailsAdapter(ArrayList<item_wallet_details> items, Context context) {
        this.itemWalletDetails = items;
        this.context = context;
    }


    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification1, null, false);
        MyHolder holder = new MyHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        item_wallet_details Item = itemWalletDetails.get(position);

        holder.notificationMonthTv.setText(Item.getNotificationMonthTv());
        holder.notificationDayTv.setText(Item.getNotificationDayTv());
        holder.notificationNumberTv.setText(Item.getNotificationNumberTv());
        holder.notificationTv.setText(Item.getNotificationTv());
        holder.notificationTimeTv.setText(Item.getNotificationTimeTv());
        holder.notificationPriceTv.setText(Item.getNotificationPriceTv());




    }

    @Override
    public int getItemCount() {
        return itemWalletDetails.size();
    }


    class MyHolder extends RecyclerView.ViewHolder {

        TextView notificationMonthTv, notificationDayTv, notificationNumberTv, notificationTv,
                notificationTimeTv, notificationPriceTv;


        public MyHolder(@NonNull View itemView) {
            super(itemView);

            notificationMonthTv = itemView.findViewById(R.id.notificationMonthTv);
            notificationDayTv = itemView.findViewById(R.id.notificationDayTv);
            notificationNumberTv = itemView.findViewById(R.id.notificationNumberTv);
            notificationTv = itemView.findViewById(R.id.notificationTv);
            notificationTimeTv = itemView.findViewById(R.id.notificationTimeTv);
            notificationPriceTv = itemView.findViewById(R.id.notificationPriceTv);


        }
    }


}
