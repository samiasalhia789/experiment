package com.samia.experiment.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.samia.experiment.R;
import com.samia.experiment.model.item_notification;

import java.util.ArrayList;


public class AdvancedRecyclerviewNotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<item_notification> items = new ArrayList<>();


    int RecyclerViewType1 = 0;
    int RecyclerViewType2 = 1;

    public AdvancedRecyclerviewNotificationAdapter(ArrayList<item_notification> items) {
        this.items = items;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        if (viewType == 0) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification1, parent, false);
            MyHolder Holder = new MyHolder(v);
            return Holder;

        } else {

            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification2, parent, false);
            MyHolder2 Holder2 = new MyHolder2(v);
            return Holder2;


        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Log.d("TAG2", items.size() + "");
        if (getItemViewType(position) == RecyclerViewType1) {
            ((MyHolder) holder).notificationDayTv.setText(items.get(position).getNotificationDayTv());
            ((MyHolder) holder).notificationMonthTv.setText(items.get(position).getNotificationMonthTv());
            ((MyHolder) holder).notificationNumberTv.setText(items.get(position).getNotificationNumberTv());
            ((MyHolder) holder).notificationPriceTv.setText(items.get(position).getNotificationPriceTv());
            ((MyHolder) holder).notificationTimeTv.setText(items.get(position).getNotificationTimeTv());
            ((MyHolder) holder).notificationTv.setText(items.get(position).getNotificationTv());


        } else {

            Log.d("TAG3", "onBindViewHolder: ");
            ((MyHolder2) holder).notification2DayTv.setText(items.get(position).getNotification2DayTv());
            ((MyHolder2) holder).notification2NumberTv.setText(items.get(position).getNotification2NumberTv());
            ((MyHolder2) holder).notification2Tv.setText(items.get(position).getNotification2Tv());

        }

    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position).getNotificationPriceTv().equals("")) {
            return RecyclerViewType2;
        } else {
            return RecyclerViewType1;
        }
    }


    class MyHolder extends RecyclerView.ViewHolder {

        //notification1
        TextView notificationMonthTv;
        TextView notificationDayTv;
        TextView notificationNumberTv;
        TextView notificationTv;
        TextView notificationTimeTv;
        TextView notificationPriceTv;

        public MyHolder(@NonNull View itemView) {
            super(itemView);

            notificationMonthTv = itemView.findViewById(R.id.notificationMonthTv);
            notificationDayTv = itemView.findViewById(R.id.notificationDayTv);
            notificationNumberTv = itemView.findViewById(R.id.notificationNumberTv);
            notificationTv = itemView.findViewById(R.id.notificationTv);
            notificationTimeTv = itemView.findViewById(R.id.notificationTimeTv);
            notificationPriceTv = itemView.findViewById(R.id.notificationPriceTv);

        }

    }

    class MyHolder2 extends RecyclerView.ViewHolder {

        //notification2
        TextView notification2DayTv;
        TextView notification2NumberTv;
        TextView notification2Tv;
        LinearLayout notificationLayout;

        public MyHolder2(@NonNull View itemView) {
            super(itemView);
            notification2DayTv = itemView.findViewById(R.id.notification2DayTv);
            notification2NumberTv = itemView.findViewById(R.id.notification2NumberTv);
            notification2Tv = itemView.findViewById(R.id.notification2Tv);
            notificationLayout = itemView.findViewById(R.id.notificationLayout);
        }

    }

}




