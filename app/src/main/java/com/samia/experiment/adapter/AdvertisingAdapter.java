package com.samia.experiment.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.samia.experiment.R;
import com.samia.experiment.activity.AdAetailsActivity;
import com.samia.experiment.activity.WalletDetailsActivity;
import com.samia.experiment.model.AdvertisingItem;
import com.samia.experiment.model.item_notification;
import com.samia.experiment.model.item_wallet_details;

import java.io.Externalizable;
import java.io.Serializable;
import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class AdvertisingAdapter extends RecyclerView.Adapter<AdvertisingAdapter.MyHolder> {

    ArrayList<AdvertisingItem> advertisingItemsn;
    Context context;
    private Activity activity;
    private Intr intr;

    public AdvertisingAdapter(ArrayList<AdvertisingItem> items, Intr intr) {
        this.advertisingItemsn = items;
        this.intr = intr;
    }


    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_advertising, null, false);
        MyHolder holder = new MyHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        AdvertisingItem Item = advertisingItemsn.get(position);
        holder.itemNameAdd.setText(Item.getItemNameAdd());
        holder.itemWatch.setText(Item.getItemWatch());
        holder.itemNumber.setText(Item.getItemNumber());
        holder.itemTime.setText(Item.getItemTime());
        holder.itemImg.setImageResource(Item.getItemImg());
        holder.OnClick(intr);

    }

    @Override
    public int getItemCount() {
        return advertisingItemsn.size();
    }

    class MyHolder extends RecyclerView.ViewHolder {
        ImageView itemImg;
        TextView itemNameAdd, itemWatch, itemNumber, itemTime;
        FrameLayout  itemFrameLayout;

        public MyHolder(@NonNull View itemView) {
            super(itemView);

            itemImg = itemView.findViewById(R.id.itemImg);
            itemNameAdd = itemView.findViewById(R.id.itemNameAdd);
            itemWatch = itemView.findViewById(R.id.itemWatch);
            itemNumber = itemView.findViewById(R.id.itemNumber);
            itemTime = itemView.findViewById(R.id.itemTime);
            itemFrameLayout = itemView.findViewById(R.id.itemFrameLayout);

        }

        public void OnClick(Intr intr) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intr.vi(getAdapterPosition());
                }
            });

        }
    }

    public interface Intr {
        void vi(int position);
    }

}
