package com.samia.experiment.model;

public class item_wallet_details {


    String notificationMonthTv;
    String notificationDayTv;
    String notificationNumberTv;
    String notificationTv;
    String notificationTimeTv;
    String notificationPriceTv;

    public item_wallet_details(String notificationMonthTv, String notificationDayTv, String notificationNumberTv, String notificationTv, String notificationTimeTv, String notificationPriceTv) {
        this.notificationMonthTv = notificationMonthTv;
        this.notificationDayTv = notificationDayTv;
        this.notificationNumberTv = notificationNumberTv;
        this.notificationTv = notificationTv;
        this.notificationTimeTv = notificationTimeTv;
        this.notificationPriceTv = notificationPriceTv;
    }


    public String getNotificationMonthTv() {
        return notificationMonthTv;
    }

    public void setNotificationMonthTv(String notificationMonthTv) {
        this.notificationMonthTv = notificationMonthTv;
    }

    public String getNotificationDayTv() {
        return notificationDayTv;
    }

    public void setNotificationDayTv(String notificationDayTv) {
        this.notificationDayTv = notificationDayTv;
    }

    public String getNotificationNumberTv() {
        return notificationNumberTv;
    }

    public void setNotificationNumberTv(String notificationNumberTv) {
        this.notificationNumberTv = notificationNumberTv;
    }

    public String getNotificationTv() {
        return notificationTv;
    }

    public void setNotificationTv(String notificationTv) {
        this.notificationTv = notificationTv;
    }

    public String getNotificationTimeTv() {
        return notificationTimeTv;
    }

    public void setNotificationTimeTv(String notificationTimeTv) {
        this.notificationTimeTv = notificationTimeTv;
    }

    public String getNotificationPriceTv() {
        return notificationPriceTv;
    }

    public void setNotificationPriceTv(String notificationPriceTv) {
        this.notificationPriceTv = notificationPriceTv;
    }
}
