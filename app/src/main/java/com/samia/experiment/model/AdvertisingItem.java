package com.samia.experiment.model;

public class AdvertisingItem {

    private int itemImg ;
    private String itemNameAdd ;
    private String itemWatch  ;
    private String itemNumber  ;
    private String itemTime  ;

    private int Id;

    public AdvertisingItem(int itemImg, String itemNameAdd, String itemWatch, String itemNumber, String itemTime, int Id) {
        this.itemImg = itemImg;
        this.itemNameAdd = itemNameAdd;
        this.itemWatch = itemWatch;
        this.itemNumber = itemNumber;
        this.itemTime = itemTime;
        this.Id = Id;
    }

    public int getItemImg() {
        return itemImg;
    }

    public void setItemImg(int itemImg) {
        this.itemImg = itemImg;
    }

    public String getItemNameAdd() {
        return itemNameAdd;
    }

    public void setItemNameAdd(String itemNameAdd) {
        this.itemNameAdd = itemNameAdd;
    }

    public String getItemWatch() {
        return itemWatch;
    }

    public void setItemWatch(String itemWatch) {
        this.itemWatch = itemWatch;
    }

    public String getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    public String getItemTime() {
        return itemTime;
    }

    public void setItemTime(String itemTime) {
        this.itemTime = itemTime;
    }

    public int getId() {
        return Id;
    }

    public void setId(int problemsId) {
        this.Id = problemsId;
    }
}