package com.samia.experiment.model;

import android.content.Context;
import android.content.SharedPreferences;

public class MySharedPreferences {
   public static  SharedPreferences prefs;
    private String sav ="com.samia.experiment.model.SAVEDATA";


    public MySharedPreferences(Context context) {
        prefs =context.getSharedPreferences(sav,Context.MODE_PRIVATE);
    }

    public static void setvalue(int number){
        prefs.edit().putInt("number",number).commit();
    }

    public static int getvalue(){
        return prefs.getInt("number",0) ;
    }
}
