package com.samia.experiment.model;

public class item_notification {
    //notification1
    String notificationMonthTv;
    String notificationDayTv;
    String notificationNumberTv;
    String notificationTv;
    String notificationTimeTv;
    String notificationPriceTv;

    //notification2
    String notification2DayTv;
    String notification2NumberTv;
    String notification2Tv;


    public item_notification(String notificationMonthTv, String notificationDayTv, String notificationNumberTv, String notificationTv, String notificationTimeTv, String notificationPriceTv, String notification2DayTv, String notification2NumberTv, String notification2Tv) {
        this.notificationMonthTv = notificationMonthTv;
        this.notificationDayTv = notificationDayTv;
        this.notificationNumberTv = notificationNumberTv;
        this.notificationTv = notificationTv;
        this.notificationTimeTv = notificationTimeTv;
        this.notificationPriceTv = notificationPriceTv;
        this.notification2DayTv = notification2DayTv;
        this.notification2NumberTv = notification2NumberTv;
        this.notification2Tv = notification2Tv;
    }

    public String getNotificationMonthTv() {
        return notificationMonthTv;
    }

    public void setNotificationMonthTv(String notificationMonthTv) {
        this.notificationMonthTv = notificationMonthTv;
    }

    public String getNotificationDayTv() {
        return notificationDayTv;
    }

    public void setNotificationDayTv(String notificationDayTv) {
        this.notificationDayTv = notificationDayTv;
    }

    public String getNotificationNumberTv() {
        return notificationNumberTv;
    }

    public void setNotificationNumberTv(String notificationNumberTv) {
        this.notificationNumberTv = notificationNumberTv;
    }

    public String getNotificationTv() {
        return notificationTv;
    }

    public void setNotificationTv(String notificationTv) {
        this.notificationTv = notificationTv;
    }

    public String getNotificationTimeTv() {
        return notificationTimeTv;
    }

    public void setNotificationTimeTv(String notificationTimeTv) {
        this.notificationTimeTv = notificationTimeTv;
    }

    public String getNotificationPriceTv() {
        return notificationPriceTv;
    }

    public void setNotificationPriceTv(String notificationPriceTv) {
        this.notificationPriceTv = notificationPriceTv;
    }

    public String getNotification2DayTv() {
        return notification2DayTv;
    }

    public void setNotification2DayTv(String notification2DayTv) {
        this.notification2DayTv = notification2DayTv;
    }

    public String getNotification2NumberTv() {
        return notification2NumberTv;
    }

    public void setNotification2NumberTv(String notification2NumberTv) {
        this.notification2NumberTv = notification2NumberTv;
    }

    public String getNotification2Tv() {
        return notification2Tv;
    }

    public void setNotification2Tv(String notification2Tv) {
        this.notification2Tv = notification2Tv;
    }

}
