package com.samia.experiment.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.samia.experiment.R;
import com.samia.experiment.activity.AboutUsActivity;
import com.samia.experiment.activity.AdvertisingActivity;
import com.samia.experiment.activity.EditprofileActivity;
import com.samia.experiment.activity.LoginActivity;
import com.samia.experiment.activity.MainActivity;
import com.samia.experiment.activity.PortfolioActivity;
import com.samia.experiment.activity.PrivacyPolicyActivity;
import com.samia.experiment.activity.TermsAndConditionsActivity;
import com.samia.experiment.activity.WalletDetailsActivity;
import com.samia.experiment.databinding.FragmentBottomSheetBinding;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BottomSheetFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BottomSheetFragment extends BottomSheetDialogFragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private FragmentBottomSheetBinding binding;
    Context context;
    Dialog dialog;

    public BottomSheetFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BottomSheetFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BottomSheetFragment newInstance(String param1, String param2) {
        BottomSheetFragment fragment = new BottomSheetFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = binding.inflate(inflater, container, false);

        dialog = new Dialog(getActivity());

        View view = binding.getRoot();


        binding.constraintLayoutWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), WalletDetailsActivity.class);
                startActivity(intent);

            }
        });

        binding.constraintLayoutLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BottomSheetToChangeLanguageFragment bottomSheetFragment = new BottomSheetToChangeLanguageFragment();
                bottomSheetFragment.show(getParentFragmentManager(), bottomSheetFragment.getTag());

            }
        });
        binding.constraintLayoutAboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), AboutUsActivity.class);
                startActivity(intent);

            }
        });

        binding.constraintLayoutPrivacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), PrivacyPolicyActivity.class);
                startActivity(intent);

            }
        });


        binding.constraintLayoutTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), TermsAndConditionsActivity.class);
                startActivity(intent);

            }
        });

        binding.constraintLayoutEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), EditprofileActivity.class);
                startActivity(intent);

            }
        });

        binding.constraintLayoutShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // show dialog
                openDialog1();

            }
        });

        return view;

    }

    private void openDialog1() {
        dialog.setContentView(R.layout.custom_dialog5);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button share = dialog.findViewById(R.id.share);
        dialog.show();


        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, "https://guides.codepath.com/android/Sharing-Content-with-Intents");
                startActivity(Intent.createChooser(shareIntent, "Share link using"));

            }
        });
       // openDialog2();
    }

    private void openDialog2() {
        dialog.setContentView(R.layout.custom_dialog4);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button dialogShareOkBtn = dialog.findViewById(R.id.dialogShareOkBtn);
        dialog.show();

        dialogShareOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }
}